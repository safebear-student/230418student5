package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class Test01_Login extends BaseTest {


    @Test
    public void testLogin() {
        //step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //step 2 click on the Login link and the Login Page loads
        welcomePage.clickOnLogin();
        //step 3 confirm that we're now on the login page
        assertTrue(loginPage.checkCorrectPage());
        // step 4 Login with valid credentials
        loginPage.login("testuser","testing");
        // step 5 Check that we're now on the User Page
        assertTrue(userPage.checkCorrectPage());
    }

}
