package com.safebear.app;

/**
 * Created by CCA_Student on 23/04/2018.
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Test01_Login.class,
})
public class TestSuite{
}
